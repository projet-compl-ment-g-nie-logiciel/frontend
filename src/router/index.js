import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'Accueil',
    component: () => import('../views/AccueilView.vue')
  },
  {
    path: '/addDocument',
    name: 'AjouterDocument',
    component: () => import('../views/AddDocumentView.vue')
  },
  {
    path: '/documents',
    name: 'Documents',
    component: () => import('../views/DocumentsView.vue')
  },
  {
    path: '/stats',
    name: 'Statistiques',
    component: () => import('../views/StatistiquesView.vue')
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'NotFound',
    component: () => import('../views/NotFoundView.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
