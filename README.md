# Projet Complément Génie Logiciel (Frontend)
Partie frontend du projet de Complément Génie Logiciel, développé avec VueJS
## Serveur local de développement
En phase de développement, utiliser le serveur local avec cette commande :
```
npm run serve
```
Accès par l'URL http://localhost:8080/

## Build
Pour obtenir un build du projet utilisable en production, exécuter cette commande :
```
npm run build
```
Build disponible dans le dossier `dist/`.

## Application conteneurisée (Docker)
- Il faut dans un premier temps build une image docker, pour cela exécuter la commande suivante (qui va se baser sur le Dockerfile) :
```
docker build -t ossacipe/projetcomplementgenielogiciel-frontend .
```
- Une fois l'image générée, on peut l'exécuter avec la commande suivante :
```
docker run -it -p 80:80 --rm --name projetcomplementgenielogiciel-frontend ossacipe/projetcomplementgenielogiciel-frontend
```
- Accès par l'URL http://localhost:80/.

## DockerHub
[Lien vers l'image stockée sur DockerHub](https://hub.docker.com/r/ossacipe/projetcomplementgenielogiciel-frontend).
